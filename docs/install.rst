Installation
============

Requirements
------------

Watchghost needs Python>=3.6 in order to work.

From source
-----------

To install Watchghost, you need to specify the extras required for watchers and loggers you want to use.


.. code-block:: shell

  # example: to install required dependencies
  # for SSH watcher and InfluxDB logger
  pip install watchghost[influx,ssh]
  watchghost

The list of extras available is:

- watchers
    - *ftp* - for FTP watcher
    - *ssh* - for SSH watcher
    - *whois* - for whois.Expire watcher
- loggers
    - *influx* - for InfluxDB logger
- others
    - *all* - to install all dependencies
    - *doc* - to generate documentation with Sphinx
    - *test* - to execute tests with pytest and selenium

Once Watchghost is installed and launched, you can access its web interface on
port ``8888``. It also installs some configuration in ``~/.config/watchghost``
if none is found.

For your distribution
---------------------

ArchLinux
~~~~~~~~~

The `latest released package <https://aur.archlinux.org/packages/watchghost/>`_ is available on AUR, as well as a package from the `master branch <https://aur.archlinux.org/packages/watchghost-git/>`_.
